// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const https = require('https')
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser')
var expressValidator = require('express-validator');
const cors = require('cors')


const app = express();
app.use(cors())

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(cookieParser());


// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// Read the link below about express behind a proxy
app.set('trust proxy', true);
app.set('trust proxy', 'loopback');


// Catch all other routes and return the index file
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

const port = process.env.PORT || 3100;

app.set('port', port);

/**
 * Create HTTP server.
 */


const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

// https.createServer(ssl, app).listen(process.env.PORT || 8443);
server.listen(port, () => console.log(`API running on localhost:${port}`));
