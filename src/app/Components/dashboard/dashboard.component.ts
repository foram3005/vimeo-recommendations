import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  shotsList:any
  videoId = 203494889;
  shotID = 0;

  constructor(public http: HttpClient) { }

  ngOnInit() {
    this.populateData();
    this.shotsList = [1,2,3,4,5,6,7,8,9,10]
  }

  getJsonData(){
    return this.http.get('http://35.188.179.37:5000/api?video_id='+this.videoId+'&shot_id='+this.shotID+'&n=10')
  }

  populateData() {
    this.getJsonData().subscribe(results => {
      console.log('results',results)
      this.shotsList = results;
    })
    //console.log(x)
  }

}