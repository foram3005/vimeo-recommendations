import { Routes, RouterModule } from '@angular/router';

//Component Declarations

import { DashboardComponent } from './components/dashboard/dashboard.component'
import { ShotsComponent } from './components/shots/shots.component';

//Guards
// import { AuthGuardService } from "./services/auth-guard/auth-guard.service";

const appRoutes: Routes = [
    { path: '', component: DashboardComponent, pathMatch: 'full' },
    { path: 'shots/:id', component: ShotsComponent, pathMatch: 'full' },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const Routings = RouterModule.forRoot(appRoutes);