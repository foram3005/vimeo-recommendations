import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable()
export class DashboardService {

  constructor(public http: HttpClient) { }

  getJSON(){
    return this.http.get('./assets/query.json')
  }

}
